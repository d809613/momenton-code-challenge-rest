# REST API for Momenton Code Challenge Solution

This REST was built by using Spring boot. It consists of 3 packages and 1 controller class:

* dao - data access interface/class for data manipulation.

* database - A simple singleton class acts as database.

* models - includes a value obejct for employee and a enum for layout types.

* CodeChallenge Controller - includes request handers for data fetching, insertion and deletion.

The executable jar is currently hosted in heroKu server for [demo](https://desolate-depths-66318.herokuapp.com/codechallenge/data) purpose.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Java 8+
* Maven 3.5.2

### Installing

Clone repo to your local system:

```
git clone https://d809613@bitbucket.org/d809613/momenton-code-challenge-rest.git
```

cd to the project folder:

```
cd momenton-code-challenge-rest
```

build dependencies:

```
mvn clean install
```

host gui app locally:

```
cd target
java -jar ./momonton-code-challenge-rest-1.0.0.jar
```

change CrossOrigin in CodeChallengeController.ORIGIN to your GUI localhost

## Deployment

This GUI app has been depoyed to a heroKu server for [demo](https://desolate-depths-66318.herokuapp.com/codechallenge/data) purpose.

## Built With

* [Spring Boot](https://projects.spring.io/spring-boot/) - Java Framework
* [Java 8](http://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html) - Multi-platform Language
* [Maven 3](https://maven.apache.org/download.cgi) - Build Tool

## Authors

* **Jing Zhou**

