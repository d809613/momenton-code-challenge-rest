package momenton;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import momenton.dao.DataCenterDAO;
import momenton.dao.DataCenterDAOImpl;
import momenton.model.Employee;
import momenton.model.LayoutType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(value = "/codechallenge")
public class CodeChallengeController {

    private static final DataCenterDAO dataCenter = new DataCenterDAOImpl();
    private static final String ALL_DATA = "/data";
    private static final String DELETE_LAYOUT_DATA = "/layout-data/delete";
    private static final String ADD_LAYOUT_DATA = "/layout-data/add/{type}";
    private static final String ORIGIN = "http://cloud.jing-momentoncodechallenge.com.s3-website-ap-southeast-2.amazonaws.com";

    // get all data
    @GetMapping(value = ALL_DATA)
    @CrossOrigin(origins = ORIGIN)
    public @ResponseBody
    Map<LayoutType, List<Employee>> getAllData() throws JsonProcessingException {
        return dataCenter.getData();
    }

    // add employee to given layout data
    @PostMapping(value = ADD_LAYOUT_DATA)
    @CrossOrigin(origins = ORIGIN)
    public @ResponseBody
    void addLayoutData(@RequestBody Employee employee, @PathVariable String type) throws JsonProcessingException {
        dataCenter.addEmployee(LayoutType.valueOf(type), employee);
    }

    // pop employee from given layout data
    @PostMapping(value = DELETE_LAYOUT_DATA)
    @CrossOrigin(origins = ORIGIN)
    public @ResponseBody
    void deleteLayoutData(@RequestBody String type) throws JsonProcessingException {
        dataCenter.deleteEmployee(LayoutType.valueOf(type));
    }

}
