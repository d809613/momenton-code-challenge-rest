package momenton.dao;


import momenton.model.Employee;
import momenton.model.LayoutType;

import java.util.List;
import java.util.Map;

/**
 * Created by jingzhou on 18/4/18.
 */
public interface DataCenterDAO {
    Map<LayoutType, List<Employee>> getData();
    List<Employee> getEmployees(LayoutType layout);
    void addEmployee(LayoutType layout, Employee employee);
    void deleteEmployee(LayoutType layout);
}
