package momenton.dao;

import momenton.database.DataWarehouse;
import momenton.model.Employee;
import momenton.model.LayoutType;

import java.util.List;
import java.util.Map;

/**
 * Created by jingzhou on 18/4/18.
 */
public class DataCenterDAOImpl implements DataCenterDAO {
    private DataWarehouse db;

    public DataCenterDAOImpl() {
        this.db = DataWarehouse.getInstance();
    }

    @Override
    public Map<LayoutType, List<Employee>> getData() {
        return db.getData();
    }

    @Override
    public List<Employee> getEmployees(LayoutType layout) {
        return db.getLayoutData(layout);
    }

    @Override
    public void addEmployee(LayoutType layout, Employee employee) {
        db.addEmployee(layout, employee);
    }

    @Override
    public void deleteEmployee(LayoutType layout) {
        db.deleteEmployee(layout);
    }
}
