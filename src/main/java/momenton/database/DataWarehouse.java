package momenton.database;


import momenton.model.Employee;
import momenton.model.LayoutType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jingzhou on 18/4/18.
 * a simple singleton DB class
 */
public class DataWarehouse {
    private static DataWarehouse instance = null;
    private Map<LayoutType, List<Employee>> data;

    protected DataWarehouse() {
        data = new HashMap<LayoutType, List<Employee >>();
        List<Employee> layoutData = new ArrayList<Employee>();

        // init table layout data
        layoutData.add(new Employee(100, "Alan", 150));
        layoutData.add(new Employee(220, "Martin", 100));
        layoutData.add(new Employee(150, "Jamie", null));
        layoutData.add(new Employee(275, "Alex", 100));
        layoutData.add(new Employee(400, "Steve", 150));
        layoutData.add(new Employee(190, "David", 400));
        data.put(LayoutType.tableLayout, layoutData);

        // init tree layout data
        layoutData = new ArrayList<Employee>();
        layoutData.add(new Employee(10, "Matt", null));
        layoutData.add(new Employee(100, "Joe", 10));
        layoutData.add(new Employee(200, "Ricardo", 100));
        layoutData.add(new Employee(300, "Sujay", 100));
        layoutData.add(new Employee(400, "Nanda", 300));
        layoutData.add(new Employee(500, "Jing", 200));
        layoutData.add(new Employee(600, "Sachin", 500));
        layoutData.add(new Employee(700, "Varun", 500));
        layoutData.add(new Employee(800, "Rashi", 600));
        layoutData.add(new Employee(900, "Rahual", 600));
        data.put(LayoutType.treeLayout, layoutData);

        // init treemap layout data
        layoutData = new ArrayList<Employee>();
        layoutData.add(new Employee(100, "Alice", null));
        layoutData.add(new Employee(200, "Mick", 100));
        layoutData.add(new Employee(300, "Bob", 200));
        layoutData.add(new Employee(400, "Frank", 200));
        layoutData.add(new Employee(500, "Amy", 200));
        layoutData.add(new Employee(600, "Izzy", 200));
        layoutData.add(new Employee(700, "Jay", 600));
        layoutData.add(new Employee(800, "Ben", 100));
        data.put(LayoutType.treemapLayout, layoutData);

        // init pack layout data
        layoutData = new ArrayList<Employee>();
        layoutData.add(new Employee(100, "John", null));
        layoutData.add(new Employee(200, "Frank", 100));
        layoutData.add(new Employee(300, "Troy", 100));
        layoutData.add(new Employee(400, "Amy", 200));
        layoutData.add(new Employee(500, "Jessie", 300));
        layoutData.add(new Employee(600, "Geoff", 500));
        layoutData.add(new Employee(700, "Vijay", 500));
        data.put(LayoutType.packLayout, layoutData);

    }
    public static DataWarehouse getInstance() {
        if(instance == null) {
            instance = new DataWarehouse();
        }
        return instance;
    }

    public void addEmployee(LayoutType layout, Employee e) {
        data.get(layout).add(e);
    }

    public void deleteEmployee(LayoutType layout) {
        List<Employee> employees = data.get(layout);
        if (employees.size() > 0) {
            employees.remove(employees.size() - 1);
        }
    }

    public Map<LayoutType, List<Employee>> getData() {
        return data;
    }

    public List<Employee> getLayoutData(LayoutType layout) {
        List<Employee> employees = data.get(layout);
        return employees;
    }
}
