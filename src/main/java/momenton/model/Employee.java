package momenton.model;

/**
 * Created by jingzhou on 18/4/18.
 */
public class Employee {
    private Integer id;
    private String name;
    private Integer manager;

    public Employee(Integer id, String name, Integer manager) {
        this.id = id;
        this.name = name;
        this.manager = manager;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getManager() {
        return manager;
    }

    public void setManager(Integer manager) {
        this.manager = manager;
    }
}
