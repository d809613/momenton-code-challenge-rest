package momenton.model;

/**
 * Created by jingzhou on 18/4/18.
 */
public enum LayoutType {
    tableLayout,
    treeLayout,
    treemapLayout,
    packLayout
}
