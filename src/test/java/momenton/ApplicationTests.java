package momenton;

import java.util.Map;

import momenton.model.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.BDDAssertions.then;

/**
 * Basic integration tests for service demo application.
 *
 * @author Jing Zhou
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = {"management.port=0"})
public class ApplicationTests {

	@LocalServerPort
	private int port;

	@Value("${local.management.port}")
	private int mgt;

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Test
	public void shouldReturn200WhenSendingGetAllDataRequestToController() throws Exception {
		@SuppressWarnings("rawtypes")
		ResponseEntity<Map> entity = this.testRestTemplate.getForEntity(
				"http://localhost:" + this.port + "/codechallenge/data", Map.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

    @Test
    public void shouldReturn200WhenSendingAddEmployeeToTableLayoutDataRequestToController() throws Exception {
        @SuppressWarnings("rawtypes")
        ResponseEntity<Map> entity = this.testRestTemplate.postForEntity(
                "http://localhost:" + this.port + "/codechallenge/layout-data/add/tableLayout", new Employee(100, "Jing", 200), Map.class);

        then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void shouldReturn200WhenSendingAddEmployeeToTreeLayoutDataRequestToController() throws Exception {
        @SuppressWarnings("rawtypes")
        ResponseEntity<Map> entity = this.testRestTemplate.postForEntity(
                "http://localhost:" + this.port + "/codechallenge/layout-data/add/treeLayout", new Employee(100, "Jing", 200), Map.class);

        then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void shouldReturn200WhenSendingAddEmployeeToTreemapLayoutDataRequestToController() throws Exception {
        @SuppressWarnings("rawtypes")
        ResponseEntity<Map> entity = this.testRestTemplate.postForEntity(
                "http://localhost:" + this.port + "/codechallenge/layout-data/add/treemapLayout", new Employee(100, "Jing", 200), Map.class);

        then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void shouldReturn200WhenSendingAddEmployeeToPackLayoutDataRequestToController() throws Exception {
        @SuppressWarnings("rawtypes")
        ResponseEntity<Map> entity = this.testRestTemplate.postForEntity(
                "http://localhost:" + this.port + "/codechallenge/layout-data/add/treemapLayout", new Employee(100, "Jing", 200), Map.class);

        then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

	@Test
	public void shouldReturn200WhenSendingPostRequestForTableLayoutDataDeletionToController() throws Exception {
		@SuppressWarnings("rawtypes")
		ResponseEntity<Map> entity = this.testRestTemplate.postForEntity("http://localhost:" + this.port + "/codechallenge/layout-data/delete", "tableLayout", Map.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	public void shouldReturn200WhenSendingPostRequestForTreeLayoutDataDeletionToController() throws Exception {
		@SuppressWarnings("rawtypes")
		ResponseEntity<Map> entity = this.testRestTemplate.postForEntity("http://localhost:" + this.port + "/codechallenge/layout-data/delete", "treeLayout", Map.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	public void shouldReturn200WhenSendingPostRequestForTreemapLayoutDataDeletionToController() throws Exception {
		@SuppressWarnings("rawtypes")
		ResponseEntity<Map> entity = this.testRestTemplate.postForEntity("http://localhost:" + this.port + "/codechallenge/layout-data/delete", "treemapLayout", Map.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}

	@Test
	public void shouldReturn200WhenSendingPostRequestForPackLayoutDataDeletionToController() throws Exception {
		@SuppressWarnings("rawtypes")
		ResponseEntity<Map> entity = this.testRestTemplate.postForEntity("http://localhost:" + this.port + "/codechallenge/layout-data/delete", "packLayout", Map.class);

		then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
	}


}
